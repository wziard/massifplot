/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

// ============================================================================
// declarations
// ============================================================================

// ----------------------------------------------------------------------------
// headers
// ----------------------------------------------------------------------------
 
// For compilers that support precompilation, includes "wx/wx.h".
#include "wx/wxprec.h"


#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// for all others, include the necessary headers (this file is usually all you
// need because it includes almost all "standard" wxWidgets headers)
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif

#include "wx/cmdline.h"

// ----------------------------------------------------------------------------
// resources
// ----------------------------------------------------------------------------

// the application icon (under Windows and OS/2 it is in resources and even
// though we could still include the XPM here it would be unused)
#if !defined(__WXMSW__) && !defined(__WXPM__)
    #include "icon.xpm"
#endif

#include "parse.h"
#include "plotsession.h"
#include "stackedgraph.h"
// ----------------------------------------------------------------------------
// private classes
// ----------------------------------------------------------------------------

// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
public:
    // override base class virtuals
    // ----------------------------

    // this one is called on application startup and is a good place for the app
    // initialization (doing it here and not in the ctor allows to have an error
    // return: if OnInit() returns false, the application terminates)
    virtual bool OnInit();
	void OnInitCmdLine(wxCmdLineParser& parser);
	bool OnCmdLineParsed(wxCmdLineParser& parser);

	wxString fileToOpen;

};


// Define a new frame type: this is going to be our main frame
class MyFrame : public wxFrame
{
public:
    // ctor(s)
    MyFrame(const wxString& title);

    // event handlers (these functions should _not_ be virtual)
    void OnQuit(wxCommandEvent &event);
    void OnAbout(wxCommandEvent &event);
    void OnOpen(wxCommandEvent &event);
    void OnScroll(wxScrollEvent &evt);
    void OnSizeChoice(wxCommandEvent &evt);
    void OnLevelChoice(wxCommandEvent &evt);
    void OnSingleChildDepthChoice(wxCommandEvent &evt);

    void DoOpen(wxString const &file);
private:
    // any class wishing to process wxWidgets events must use this macro
    DECLARE_EVENT_TABLE()

	Session *session;
	SessionPlot *sessionPlot;
	StackedGraph *stackedGraph;
	wxScrollBar *theScrollBar;
	wxChoice *sizeChoice, *levelChoice, *singleChildDepthChoice;

	unsigned int maxSize, maxLevel;
};

// ----------------------------------------------------------------------------
// constants
// ----------------------------------------------------------------------------

// IDs for the controls and the menu commands
enum
{
    // menu items
    Msplot_Quit = wxID_EXIT,
    Msplot_Open = wxID_OPEN,


    // it is important for the id corresponding to the "About" command to have
    // this standard value as otherwise it won't be handled properly under Mac
    // (where it is special and put into the "Apple" menu)
    Msplot_About = wxID_ABOUT,
    Msplot_SizeChoice = 0,
    Msplot_LevelChoice,
    Msplot_SingleChildDepthChoice,
    Msplot_last
};




// ----------------------------------------------------------------------------
// event tables and other macros for wxWidgets
// ----------------------------------------------------------------------------

// the event tables connect the wxWidgets events with the functions (event
// handlers) which process them. It can be also done at run-time, but for the
// simple menu events like this the static method is much simpler.
BEGIN_EVENT_TABLE(MyFrame, wxFrame)
    EVT_MENU(Msplot_Quit,  MyFrame::OnQuit)
    EVT_MENU(Msplot_About, MyFrame::OnAbout)
    EVT_MENU(Msplot_Open, MyFrame::OnOpen)
    EVT_CHOICE(Msplot_SizeChoice, MyFrame::OnSizeChoice)
    EVT_CHOICE(Msplot_LevelChoice, MyFrame::OnLevelChoice)
    EVT_CHOICE(Msplot_SingleChildDepthChoice, MyFrame::OnSingleChildDepthChoice)
    EVT_COMMAND_SCROLL(wxID_ANY, MyFrame::OnScroll)
END_EVENT_TABLE()

// Create a new application object: this macro will allow wxWidgets to create
// the application object during program execution (it's better than using a
// static object for many reasons) and also implements the accessor function
// wxGetApp() which will return the reference of the right type (i.e. MyApp and
// not wxApp)
IMPLEMENT_APP(MyApp)

// ============================================================================
// implementation
// ============================================================================

// ----------------------------------------------------------------------------
// the application class
// ----------------------------------------------------------------------------



// 'Main program' equivalent: the program execution "starts" here
bool MyApp::OnInit()
{
    // call the base class initialization method, currently it only parses a
    // few common command-line options but it could be do more in the future
    if ( !wxApp::OnInit() )
        return false;

    // create the main application window
    MyFrame *frame = new MyFrame(_T("Msplot wxWidgets App"));

    // and show it (the frames, unlike simple controls, are not shown when
    // created initially)
    frame->Show(true);


    if (fileToOpen.Len())
    {
    	frame->DoOpen(argv[1]);
    }

    // success: wxApp::OnRun() will be called which will enter the main message
    // loop and the application will run. If we returned false here, the
    // application would exit immediately.
    return true;
}

// ----------------------------------------------------------------------------
// main frame
// ----------------------------------------------------------------------------

// frame constructor
MyFrame::MyFrame(const wxString& title)
       : wxFrame(NULL, wxID_ANY, title, wxDefaultPosition, wxSize(640,480))
{
    // set the frame icon
    SetIcon(wxICON(sample));

#if wxUSE_MENUS
    // create a menu bar
    wxMenu *fileMenu = new wxMenu;

    // the "About" item should be in the help menu
    wxMenu *helpMenu = new wxMenu;
    helpMenu->Append(Msplot_About, _T("&About...\tF1"), _T("Show about dialog"));

    fileMenu->Append(Msplot_Open, _T("&Open\tAlt-O"), _T("Open massif file"));
    fileMenu->Append(Msplot_Quit, _T("E&xit\tAlt-X"), _T("Quit this program"));

    // now append the freshly created menu to the menu bar...
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append(fileMenu, _T("&File"));
    menuBar->Append(helpMenu, _T("&Help"));

    // ... and attach this menu bar to the frame
    SetMenuBar(menuBar);

#endif // wxUSE_MENUS



	wxPanel *bgPanel = new wxPanel(this, -1);
	wxSizer *theSizer = new wxBoxSizer(wxVERTICAL);

	bgPanel->SetSizer(theSizer);


	wxSizer *topSizer = new wxBoxSizer(wxHORIZONTAL);


	theSizer->Add(topSizer, wxSizerFlags(0).Expand());

	sizeChoice = new wxChoice(bgPanel, Msplot_SizeChoice);
	sizeChoice->Append(wxT("Static scaling"));
	sizeChoice->Append(wxT("Dynamic scaling"));
	sizeChoice->SetSelection(0);

	levelChoice = new wxChoice(bgPanel, Msplot_LevelChoice);
	levelChoice->Append(wxT("Static radius"));
	levelChoice->Append(wxT("Dynamic radius"));
	for (int i = 0; i < 20; i++)
	{
		levelChoice->Append(wxString::Format(wxT("%d"),i+1));
	}
	levelChoice->SetSelection(0);

	singleChildDepthChoice = new wxChoice(bgPanel, Msplot_SingleChildDepthChoice);
	for (int i = 0; i < 20; i++)
	{
		singleChildDepthChoice->Append(wxString::Format(wxT("%d"),i+1));
	}

	singleChildDepthChoice->SetSelection(0);

	topSizer->Add(sizeChoice, wxSizerFlags(1));

	topSizer->Add(levelChoice,wxSizerFlags(1));

	topSizer->Add(singleChildDepthChoice,wxSizerFlags(1));
	
	sessionPlot = new SessionPlot(bgPanel);

	theSizer->Add(sessionPlot, wxSizerFlags(1).Expand());

	theScrollBar = new wxScrollBar(bgPanel, -1);

	theSizer->Add(theScrollBar, wxSizerFlags().Expand());

	stackedGraph = new StackedGraph(bgPanel);

	theSizer->Add(stackedGraph, wxSizerFlags(0).Expand());


#if wxUSE_STATUSBAR
    // create a status bar just for fun (by default with 1 pane only)
    CreateStatusBar(2);
    SetStatusText(_T("Welcome to wxWidgets!"));
#endif // wxUSE_STATUSBAR

	session = NULL;


}


// event handlers

void MyFrame::OnQuit(wxCommandEvent& WXUNUSED(event))
{
    // true is to force the frame to close
    Close(true);
}


void MyFrame::DoOpen(wxString const &file)
{
	FILE *in = fopen((const char *)(file.mb_str(wxConvUTF8)), "r");

	if (!in)
	{
		return;
	}

	session = ReadMassifFile(in);

	//session->DumpXML();

	sessionPlot->SetSession(session);

	theScrollBar->SetScrollbar(0, 1, session->GetNumSnapShots() , 1, true);


	fclose(in);

	maxSize  = session->GetMaxSize();
	maxLevel = session->GetMaxDepth(1);

	sessionPlot->SetMaxValue(maxSize);
	sessionPlot->SetMaxLevel(maxLevel);
	

	wxString statusText;
	statusText.Printf(wxT("Opened %d snapshots, max size %u"), session->GetNumSnapShots(), maxSize);
	SetStatusText(statusText);
}

void MyFrame::OnOpen(wxCommandEvent& WXUNUSED(event))
{
	wxString file = wxFileSelector(wxT("Open Massif file"), wxT(""), wxT(""), wxT(""), wxT("massif.*"), wxFD_OPEN | wxFD_FILE_MUST_EXIST);




	if (file.IsEmpty())
		return;


	DoOpen(file);

}



void MyFrame::OnAbout(wxCommandEvent& WXUNUSED(event))
{
    wxMessageBox(wxString::Format(
                    _T("Welcome to %s!\n")
                    _T("\n")
                    _T("This is the minimal wxWidgets sample\n")
                    _T("running under %s."),
                    wxVERSION_STRING,
                    wxGetOsDescription().c_str()
                 ),
                 _T("About wxWidgets minimal sample"),
                 wxOK | wxICON_INFORMATION,
                 this);
}


void MyFrame::OnScroll(wxScrollEvent &WXUNUSED(evt))
{
	unsigned int pos = theScrollBar->GetThumbPosition();

	sessionPlot->SetSnapShot(pos);
}


void MyFrame::OnSizeChoice(wxCommandEvent &evt)
{
	sessionPlot->SetMaxValue(evt.GetSelection() ? 0 : maxSize);
}

void MyFrame::OnSingleChildDepthChoice(wxCommandEvent &evt)
{
	unsigned int scd = evt.GetSelection() + 1;

	sessionPlot->SetSingleChildDepth(scd);

	if (levelChoice->GetSelection() == 0)
	{
		maxLevel = session->GetMaxDepth(scd);
		sessionPlot->SetMaxLevel(maxLevel);
	}
}



void MyFrame::OnLevelChoice(wxCommandEvent &evt)
{
	int level = evt.GetSelection();

	if (level == 0)
	{
		level = maxLevel;
	}

	if (level == 1)
	{
		level = 0;
	}


	if (level > 1)
	{
		level -= 1;
	}


	sessionPlot->SetMaxLevel(level);
}


static const wxCmdLineEntryDesc gCmdLineDesc[] =
{
       { wxCMD_LINE_SWITCH, wxT("h"), wxT("help"), wxT("Display usage info"), wxCMD_LINE_VAL_NONE, wxCMD_LINE_OPTION_HELP },
       { wxCMD_LINE_PARAM, NULL, NULL, wxT("File to open"), wxCMD_LINE_VAL_STRING, wxCMD_LINE_PARAM_OPTIONAL},
       { wxCMD_LINE_NONE, NULL, NULL, NULL, wxCMD_LINE_VAL_NONE, 0}, 
}; 

void MyApp::OnInitCmdLine(wxCmdLineParser& parser)
{
        parser.SetDesc(gCmdLineDesc);
        parser.SetSwitchChars(wxT("-"));
}

bool MyApp::OnCmdLineParsed(wxCmdLineParser& parser)
{


        if (parser.GetParamCount())
        {
                fileToOpen = parser.GetParam(0);
        }
#ifdef DEV_DEBUG
        wxString script;
        if (parser.Found(wxT("s"), &script))
        {
                printf( "script >%s<\n", static_cast<const char *>(script.c_str()) );
        }
		if (parser.Found(wxT("t")))
		{
			testMode = true;
		}

		long found;
		if (parser.Found(wxT("f"), &found))
		{
			filterToRun = found;
		}
		if (parser.Found(wxT("p"), &found))
		{
			presetToRun = found;
		}

#endif
        return true;
}

