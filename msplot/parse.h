/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#ifndef __PARSE_H__
#define __PARSE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "crc/crc.h"
// parse.cpp and parse.h implement the datastructures to hold a massif session, and the routines to parse it from a massif output file



// TreeNode trees are used to store massif's callback trees
class TreeNode
{
	public:
		TreeNode(int _size, char const *_name, bool insig)
		{
			parent = NULL;
			size = _size;
			name = strdup(_name);
			numChildren = maxNumChildren = 0;
			children = NULL;
			id = 0;
			isInsignificant = insig;

			// these two are set once the entire snapshot tree is read in
			numLeaves = 0; // the number of leaves below this node
			branchDepth = 0; // the distance so the first parent which has more than 1 child
		}
		~TreeNode()
		{
			delete [] children;
			free(name);
		}

		int CalcNumLeavesAndSetBranchDepth()
		{
			if (!parent | (numChildren > 1))
			{
				branchDepth = 0;
			}
			else
			{
				branchDepth = parent->branchDepth + 1;
			}

			if (!numChildren)
			{
				return 1;
			}

			numLeaves = 0;
			for (int i = 0; i < numChildren; i++)
			{
				numLeaves += children[i]->CalcNumLeavesAndSetBranchDepth();
			}

			return numLeaves;
		}

		
		void AddChild(TreeNode *c)
		{
			if (numChildren >=  maxNumChildren)
				Grow();


			if (strstr(c->name, "all below massif's threshold"))  // add at the end
			{
				children[numChildren] = c;
			}
			else	// add alphapetically
			{
				int  i = 0;
				while (i < numChildren && (strcmp(children[i]->name, c->name) < 0) && !((i == numChildren -1) && strstr(children[i]->name, "all below massif's threshold")))
				{
					i++;
				}
	
				for (int j = numChildren; j > i; j--)
				{
					children[j] = children[j-1];
				}
				
	
	
				children[i] = c;
			}
			numChildren++;
			c->parent = this;
		}

		//GetId() returns a unique id for each treenode, which is gueranteed to be the same for
		//different snapshots. It returns 0 before Enumerate is called.
		unsigned int GetId() const
		{
			if (!id)
			{
				id = GenerateId();
			}
			return id;
		}


		unsigned int GenerateId() const
		{
			char buf[1024];

			// create a 'path' for this node, consisting of its name and it's parent's name and that parents name etc concatenated with / between

			// maybe rewrite one day using std::string?
			int pos = 0;

			const TreeNode *t = this;
			int length = 0;

			while (t)
			{
				int l = strlen(t->name);
				if ( pos + l + 2 > 1024)
				{
					length = pos;
					break;
				}
				memcpy(buf + pos, t->name, l);
				pos += l;

				buf[pos]= '/';
				pos++;

				t = t->parent;

				if (!t)
				{
					length = pos;
				}
			}

			// now calc a crc from this path

			return mycrc(buf, length);

		}

		TreeNode const *GetParent() const { return parent; }

		TreeNode const *FindNode(unsigned int idToFind, TreeNode *skip = NULL) const
		{
			if (id == idToFind  && this != skip)
			{
				return this;
			}

			TreeNode const *ret;
			for (int i = 0; i < numChildren; i++)
			{
				ret = children[i]->FindNode(idToFind);

				if (ret)
					return ret;
			}

			return NULL;
		}

		// clone this node and it's children
		TreeNode *Clone() const
		{
			TreeNode *ret = new TreeNode(size, name, isInsignificant);
			ret->id =id;
			ret->branchDepth = branchDepth;
			ret->numLeaves = numLeaves;
			for (int i = 0; i < numChildren; i++)
			{
				ret->AddChild(children[i]->Clone());
			}

			return ret;
		}

		// this is used to build up the global tree
		//  it checks if this node already contains a child of the same name, and if so recursively merges the children
		// or if it doen's yet exist it clones them
		void MergeChild(TreeNode const *t);

		unsigned int GetNumChildren() const { return numChildren; }
		TreeNode *const GetChild(int i) const { return children[i]; }
		char const *GetName() const { return name; }

		unsigned int GetSize() const { return size; }

		void DumpXML(int leadingSpaces)
		{
			for (int i = 0; i < leadingSpaces; i++)
				printf(" ");

			printf("<treenode memsize=\"%d\" name=\"%s\"  branchdepth=\"%d\" numleaves=\"%d\">\n", size, name, branchDepth, numLeaves);
			for (int i = 0; i < numChildren; i++)
			{
				children[i]->DumpXML(leadingSpaces+1);
			}
			
			for (int i = 0; i < leadingSpaces; i++)
				printf(" ");
			printf("</treenode>\n");
			
		}


		void FindMaxDepth(unsigned int maxSingleLeafDepth, unsigned int level, unsigned int *depth) const
		{

			if (level > *depth)
				*depth = level;

			if (numLeaves == 1  && branchDepth >= maxSingleLeafDepth)
			{
				return;
			}

			for (int i = 0; i < numChildren; i++)
			{
				children[i]->FindMaxDepth(maxSingleLeafDepth,  level +1, depth);
			}

		}

		unsigned int GetNumLeaves() const { return numLeaves; }
		unsigned int GetBranchDepth() const { return branchDepth; }
		
	private:
		void Grow()
		{
			if (children)
			{
				maxNumChildren *=2;
				TreeNode **nw = new TreeNode *[maxNumChildren];
				for (int i = 0; i< numChildren ;i++)
				{
					nw[i] = children[i];
				}
				delete [] children;
				children =nw;
			}
			else
			{
				maxNumChildren = 1;
				children = new TreeNode *[maxNumChildren];
			}
		}
		int numChildren;
		int maxNumChildren;
		TreeNode **children;

		unsigned int numLeaves;
		unsigned int branchDepth;

		mutable unsigned int id;
		int size;
		char *name;
		TreeNode *parent;
		bool isInsignificant;
};


// a Snapshot holds all information of a single massif snapshot
class SnapShot
{
	public:
		SnapShot(unsigned int _nr)
		{
			nr = _nr;
			time = 0;
			heap = 0;
			heap_extra = 0;
			stack = 0;
			allocTree = NULL;
			dummy = new TreeNode(0, "no tree available", true);
		}

		void SetTime(unsigned int t)
		{
			time = t;
		}

		void SetHeap(unsigned int h)
		{
			heap = h;
			if (dummy)
				delete dummy;
			dummy = new TreeNode(heap, "no tree available", true);
		}

		void SetHeapExtra(unsigned int h)
		{
			heap_extra = h;
		}

		void SetStack(unsigned int s)
		{
			stack = s;
		}

		void AddAllocTree(TreeNode *t)
		{
			allocTree = t;
			t->CalcNumLeavesAndSetBranchDepth();
		}

		
		~SnapShot()
		{
			if (allocTree)
				delete allocTree;

			delete dummy;
		}

		TreeNode const *GetAllocTree() const
		{
			if (allocTree)
				return allocTree;


			return dummy;
		}

		void DumpXML()
		{
			printf(" <snapshot nr=\"%d\" time=\"%d\" heap=\"%d\" heap_extra=\"%d\" stack=\"%d\" alloctree=\"%s\">\n", nr, time, heap, heap_extra, stack, allocTree ? "detailed" : "empty" );

			if (allocTree)
				allocTree->DumpXML(2);
			printf(" </snapshot>\n");
		}

		unsigned int GetMaxDepth(unsigned int maxSingleLeafDepth)
		{
			if (!allocTree)
				return 0;

			unsigned int depth = 0;

			allocTree->FindMaxDepth(maxSingleLeafDepth,  1, &depth);

			return depth;
		}

		TreeNode const *FindNode(unsigned int id)
		{
			TreeNode const *ret = NULL;
			if (allocTree)
			{
				ret = allocTree->FindNode(id);
			}

			return ret ? ret : dummy;
		}

	private:

		unsigned int nr, time, heap, heap_extra, stack;
		TreeNode *allocTree;
		TreeNode *dummy;

};

// this holds all information of a massif outputfile
class Session
{
	public:
	Session()
		: collectedTree(0,"top", true)
	{
		desc = NULL;
		cmd = NULL;
		time_unit = NULL;

		snapShots = NULL;
		numSnapShots = maxNumSnapShots = 0;
	}

	void SetDesc(char const *s)
	{
		desc = strdup(s);
	}

	void SetCmd(char const *s)
	{
		cmd = strdup(s);
	}

	void SetTimeUnit(char const *s)
	{
		time_unit = strdup(s);
	}

	~Session()
	{
		free(desc);
		free(time_unit);
		free(cmd);

		if (snapShots)
		{
			for (int  i =0; i < numSnapShots; i++)
			{
				delete snapShots[i];
			}
			
			delete [] snapShots;
		}
	}

	void AddSnapShot(SnapShot *s)
	{
		if (numSnapShots >= maxNumSnapShots)
			Grow();

		snapShots[numSnapShots++] = s;

		collectedTree.MergeChild(s->GetAllocTree());
	}


	void DumpXML()
	{
		printf("<session cmd=\"%s\" desc=\"%s\" time_unit=\"%s\">\n", cmd, desc, time_unit);
		for (int i = 0; i < numSnapShots; i++)
		{
			snapShots[i]->DumpXML();
		}
		printf("</session>");
	}


	SnapShot const * GetSnapShot(unsigned int s)
	{
		return snapShots[s];
	}

	unsigned GetNumSnapShots() { return numSnapShots; }


	unsigned int GetMaxSize()
	{
		unsigned int ret = 0, s = 0;
		for (int i = 0; i < numSnapShots; i++)
		{
			s = snapShots[i]->GetAllocTree()->GetSize();

			if (s > ret)
				ret = s;
		}

		return ret;
	}
	

	unsigned int GetMaxDepth(unsigned int maxSingleLeafDepth)
	{
		unsigned int ret = 0, l;
		for (int i = 0; i < numSnapShots; i++)
		{
			l = snapShots[i]->GetMaxDepth(maxSingleLeafDepth);

			if (l > ret)
				ret = l;
		}

		return ret;
	}


	void DoFinalBookKeeping() // called after the file has been parsed to scan the tree
	{
		// calc maxmem and maxlevel as well here?
		// maybe check for dublicate id's here?
//		collectedTree.DumpXML(0);
	}

	TreeNode const *FindNode(unsigned int snapShot, unsigned int id)
	{
		return snapShots[snapShot]->FindNode(id);
	}
	
	private:

	char *desc, *cmd, *time_unit;
	int numSnapShots, maxNumSnapShots;
	void Grow()
	{
		if (!maxNumSnapShots)
		{
			maxNumSnapShots = 64;
		}
		else
		{
			maxNumSnapShots *= 2;
		}

		SnapShot **tmp = new SnapShot *[maxNumSnapShots];

		for (int i = 0; i < numSnapShots; i++)
			tmp[i] = snapShots[i];

		delete [] snapShots;

		snapShots = tmp;
	}

	SnapShot ** snapShots;

	TreeNode collectedTree; // this tree is build up by merging all snapshot trees, and thus contains all nodes that exist in the log, it is used for enumeration of the nodes



	
};




extern Session *ReadMassifFile(FILE *in);

#endif
