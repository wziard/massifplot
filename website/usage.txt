Warning
================================
This is very much ALPHA software. While it might be useful to most valgrind users, since valgrind users will probably be developers and have some experience in compiling experimental stuff, it is not very finished and so might be a complete waste of your time. You have been warned ;-)

Purpose
================================
Generate (somewhat) easy to understand graphical representations from massifs logfile.

Why
================================
With the textual graphs massif produces it is very hard to see the 'big picture'. In a graphical representation it is much easier to quickly spot where your program is using all it's memory.

Inspiration
================================
The tree structure of massif's output is in a way similar to the directories and subdirectories on a disk wher each subdirectory uses some amount of memory. Since I always liked programs like filelight which display this information in a sort of 'nested piegraph' I tried to use the same visualisation method for eacht valgrind snapshot.

Building
================================
- you need wxwidgets (www.wxwidgets.org) 2.8 , the default unicode 2.8 on ubuntu 7.10 or 8.04 should be fine

 unpack the tgz.
 Go into the msplot dir and run 'make fixbuild && make'  copy the resulting 'msplot' executable to /usr/local/bin
 p.s.
 if you want to hack on the program itself, it might be handy to run 'make depend' to fix the dependencies in the makefile.


Usage
================================

run your program under massif with the following options:

valgrind --tool=massif --max-snapshots=1000 --detailed-freq=1	   <your-program>

open the file with

msplot massif.out.<pid>

Description of the main window
=================================

At the bottom you see a scrollbar. With this you can scroll through the time axis of your program. For each snapshot a piegraph will be plotted.
In the upper left you see the name of the function corresponding to the pie segment the mouse currenly hovering.
At the bottom you see the amount of memory which corresponds to one full circle of the pie chart
At the top of the window you can chose different scaling options. The dynamic options will renormalise the graph for every snapshot, for the static options the same scaling will be used for all snapshots.

In the middle you can see the pie chart itself.

When you click on a segment it is (semi-)permanently labelled on the left side of the window.

When you doubleclick on a segment this segmend will be put in the center and the graph will be 'zoomed in'


Plans
=================================
- better docs ;-)
- more graph types
- nicer graphics
- xml loading/saving of annotated sessions?

Problems
=================================
- The massif file format is not guaranteed to stay the same between valgrind versions. For this reason I didn't waste much time an a very advanced/flexible parser but made something simple which does the job.
- The usability should be much better ;-)

How to reach me
=================================
I try to read the Valgrind-users mailing list. But I cannot keep up mosyt of the time.
You can mail me at msplot@aaltjegron.nl with suggestions/patches.
