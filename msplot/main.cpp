//    This file is part of msplot
//    Copyright 2008, 2009 Martijn Versteegh
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>

#include "parse.h"



int main(int argc, char **argv)
{

	if (argc !=2)
	{
		printf("usage:\n %s <massif-outputfile>\n", argv[0]);
		exit(1);
	}


	FILE *in = fopen(argv[1], "r");

	if (!in)
	{
		printf("error opening file %s\n", argv[1]);
		exit(1);
	
	}


	Session *theSession = ReadMassifFile(in);

	theSession->DumpXML();

	fclose(in);
}
