#include <ctype.h>
#include <string.h>
#include "parser.h"


XMLTree::XMLTree()
{
    name = value = NULL;
    children = NULL;
    maxNumChildren = numChildren = 0;
    attributes = NULL;
    maxNumAttributes = numAttributes = 0;
    parent = NULL;


}

XMLTree::~XMLTree()
{
    int i;

    if (name)
        free(name);

    if (value)
        free(value);

    if (numChildren)
    {
        for (i = 0; i< numChildren; i++)
        {
            delete children[i];
        }
    }
    if (children)
        free(children);

    if (numAttributes)
    {
        for (i = 0; i< numAttributes; i++)
        {
            delete attributes[i];
        }
    }
    if (attributes)
        free(attributes);
        

}

void XMLTree::SetName(char const *n, int nameSize)
{
    if (name)
        free(name);

    if (!nameSize)
        nameSize = strlen(n);

    name = (char *)malloc((nameSize+1) * sizeof(char));

    name[nameSize] = 0;
    strncpy(name, n, nameSize);
}

char const *XMLTree::GetName()  const
{
    return name ? name : "";
}

void XMLTree::AppendValue(char const *v, int size)
{
    if (!size)
        size = strlen(v);
        
    if (!value)
    {
        value = (char *)malloc(size +1);
        value[size] = 0;
        strncpy(value, v, size);
    }
    else
    {
        int osize = strlen(value);

        char *nwv = (char *)malloc(osize + size + 1);
        nwv[osize+size] = 0;
        strncpy(nwv, value, osize);
        strncpy(nwv+osize, v, size);

        free(value);
        value = nwv;
        
    }
}

char const *XMLTree::GetAttribute(char const *att) const
{
    XMLTree *a = FindAttribute(att);

    if (!a)
        return NULL;

    return a->GetValue();
}

void XMLTree::SetAttribute(char const *att, char const *value)
{
    XMLTree *a = FindAttribute(att);

    if (a)
        a->SetValue(value);
    else
        AddAttribute(att, value);

}


void XMLTree::ClearValue()
{
    if (value)
        free(value);

    value = NULL;
}

char const *XMLTree::GetValue()  const
{
    return value ? value : "";
}

void XMLTree::AppendChild(XMLTree *child)
{
    assert(child->parent == NULL);

    if (numChildren >= maxNumChildren)
        GrowChildren();

    children[numChildren] = child;

    child->parent = this;
    
    numChildren++;


}


int XMLTree::GetNumChildren()  const
{
    return numChildren;
}

XMLTree *XMLTree::GetChild(int i)
{
    assert(i < numChildren);

    return children[i];
}

void XMLTree::RemoveChild(int c)
{
    assert(c < numChildren);

    children[c]->parent = NULL;

    for (int i = c; i< numChildren-1; i++)
        children[i] = children[i+1];

    numChildren--;

    children[numChildren] = NULL;
    
}


void XMLTree::PrintAttributes(FILE *to)  const
{
    int i;
    for (i = 0; i<numAttributes; i++)
    {
        fprintf(to, " %s=\"", attributes[i]->GetName());
        PrintEscaped(to, attributes[i]->GetValue());
        fputc('"', to);
    }
}


void XMLTree::Print(FILE *to, bool onlyChildren, int indent, bool autoIndent)  const
{
    if (!onlyChildren)
    {
        for (int i = 0; i< indent; i++)  fputc(' ', to);

        fputc('<', to);
        PrintEscaped(to, GetName());
        PrintAttributes(to);
        fputc('>', to);
        
        
        if (indent || autoIndent)
            fprintf(to, "\n");
            
        if (HasValue())
        {
            for (int i = 0; i< indent; i++)  fputc(' ', to);
            if (autoIndent)
                fprintf(to, " ");
            
            PrintEscaped(to, GetValue());
            if (indent || autoIndent)
                fprintf(to, "\n");
        }
    }
    for (int i = 0; i< numChildren; i++)
    {
        children[i]->Print(to, false, ((!onlyChildren) && autoIndent) ? indent +1 : indent, autoIndent);
    }
    if (!onlyChildren)
    {
        for (int i = 0; i< indent; i++)  fputc(' ', to);
        fputc('<', to);
        fputc('/', to);
        PrintEscaped(to, GetName());
        fputc('>', to);
        
        if (indent || autoIndent)
            fprintf(to, "\n");
    }
}

XMLTree *XMLTree::GetParent()  const
{
    return parent;
}

void XMLTree::GrowChildren()
{
    maxNumChildren +=10;

    if (children)
        children = (XMLTree**)realloc(children, maxNumChildren * sizeof(XMLTree **));
    else
        children = (XMLTree**)malloc(maxNumChildren * sizeof(XMLTree **));
    

    assert(children);

}


void XMLTree::GrowAttributes()
{
    maxNumAttributes +=10;

    if (attributes)
        attributes = (XMLTree**)realloc(attributes, maxNumAttributes * sizeof(XMLTree **));
    else
        attributes = (XMLTree**)malloc(maxNumAttributes * sizeof(XMLTree **));
    

    assert(attributes);
}

void XMLTree::AddAttribute(char const *a, char const *v)
{
    if (numAttributes >= maxNumAttributes)
        GrowAttributes();

    attributes[numAttributes] = new XMLTree;
    attributes[numAttributes]->SetName(a);
    attributes[numAttributes]->SetValue(v);
    numAttributes++;
}

XMLTree *XMLTree::FindAttribute(char const *a) const
{
    int i;
    for (i = 0; i< numAttributes; i++)
    {
        if (!strcmp(a, attributes[i]->GetName()))
            return attributes[i];
    }

    return NULL;
}



bool CMyXML::ParseCString(char const * buffer, bool iw, bool isLast)
{
    ignoreWhiteSpace = iw;

    int nLength = strlen(buffer); // needs to be correct for utf8
    char * pszBuffer = (char *) GetBuffer (nLength); // REQUEST
    memcpy(pszBuffer, buffer, nLength);
    

    return ParseBuffer (nLength, isLast); // PARSE

}


bool CMyXML::ParseFile (FILE *fp, bool iw)
{
    ignoreWhiteSpace = iw;

    if (!Create ())
        return false;
    
    
    // Loop while there is data
    
    bool fSuccess = true;
    while (!feof (fp) && fSuccess)
    {
        char * pszBuffer = (char *) GetBuffer (256); // REQUEST
        if (pszBuffer == NULL)
            fSuccess = false;
        else
        {
            int nLength = fread (pszBuffer, 1, 256, fp); // READ
            fSuccess = ParseBuffer (nLength, nLength == 0); // PARSE
        }   
    }

    return fSuccess;
}

XMLTree *XMLTree::FindOrCreateChild(char const *name, int nameSize)
{
    XMLTree *found = FindChild(name, nameSize);

    if (!found)
    {
        found = new XMLTree;

        found->SetName(name, nameSize);
        AppendChild(found);
    }

    return found;
    
}

XMLTree *XMLTree::FindChild(char const *name, int nameSize)  const
{
    XMLTree *found = NULL;

    if (!nameSize)
        nameSize = strlen(name);
        
    for (int i = 0; i < numChildren; i++)
    {
        if (!strncmp(children[i]->GetName(), name, nameSize))
        {
            found = children[i];
            break;
        }
    }
    return found;
}

void  XMLTree::PrintEscaped(FILE *to, char const *s) const
{
        while (*s)
        {
            switch( *s ) {
            case '&': fprintf(to, "&amp;"); break;
            case '<': fprintf(to,"&lt;"); break;
            case '>': fprintf(to, "&gt;"); break;
            case '"': fprintf(to,"&quot;"); break;
            default:  fputc(*s, to); break;
            }
            s++;
        }
}

void XMLTree::Trim()
{
    if (!value)
        return;

    char *s = value;
    int l = strlen(value);
    char *e = s + l - 1;

    while(*s && isspace(*s))
        s++;

    if (!(*s))
    {
        free(value);
        value = NULL;
        return;
    }

    while(isspace(*e))
        e--;

    e[1] = 0;

    char *nwv = strdup(s);

    free(value);

    value = nwv;
}

