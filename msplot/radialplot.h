/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/



// this file implements a RadialPlot widget for wxWidgets, it doesn't depend on any other part of msplot, so you can easily re-use it
// for other projects.

// wxWidgets drawing code sucks though, so it doesn't really look to good.

#ifndef __RADIALPLOT_H__
#define __RADIALPLOT_H__

#include "wx/window.h"
#include "wx/bitmap.h"
#include "wx/dcclient.h"
#include "wx/dcmemory.h"
#include <math.h>

#include "radialplottreeitem.h"
#include "wxcairo.h"


class RadialPlot
	: public wxWindow
{
	public:
		RadialPlot(wxWindow *parent)
			: wxWindow(parent,-1)
		{
			maxNumItems = 32;
			numItems = 0;
			items = new RadialPlotTreeItem[maxNumItems];
			backBuffer = NULL;
			maxLevel = 0;
			maxValue = 0;
			autoLevel = false;
			selected = (unsigned int)(-1);
			#ifdef USE_CAIRO
			surface = NULL;
			context = NULL;
			#endif

		}
		
		virtual ~RadialPlot()
		{
			delete [] items;
			delete backBuffer;
			#ifdef USE_CAIRO
			cairo_destroy(context);
			cairo_surface_destroy(surface);
			#endif
		}
		
		
		void RefreshTree(void const *startNode, int _maxLevel, int _maxValue);

		wxColor GetItemColor(void const *handle)
		{
			unsigned int item = FindItem(handle);

			if (item == (unsigned int)(-1))
				return wxColor(0,0,0);

			return items[item].color;
		}

		wxColor GetItemColor(unsigned int id)
		{
			unsigned int item = FindItem(id);

			if (item == (unsigned int)(-1))
				return wxColor(0,0,0);

			return items[item].color;
		}

		wxPoint GetItemCentre(void const *handle);
		wxPoint GetItemCentre(unsigned int id);

		virtual void OnMouseEvt(wxMouseEvent &event, void const *handle)	// pass mouse events with prefilled item handle
		{
		
		}
		// these get called with e DC associated with the backbuffer bitmap (the control is
		// drawn double-buffered for easy refreshing/anti-flickering)
		// before and after the graph is drawn
		// you can use this to add your own stuff under or over the graph
		virtual void  CustomDrawPre(wxDC *bitmap)
		{
		}
		
		virtual void  CustomDrawPost(wxDC *bitmap)
		{

		}

		// you can override this function to give each element a specific color
		//the default will derive a color from  the id
		virtual wxColor GenerateElementColor(void const *handle)
		{
			unsigned int id = GetId(handle);
			return wxColor(GenerateColor(id));
		}

		// overrid this to generate different colors for an id
		// not that it's no use to override this function if you already
		// derived an ElementColor function, since then this function won't be called
		virtual unsigned int GenerateColor(unsigned int c);
		
	private:
		DECLARE_EVENT_TABLE();

		void OnMouse(wxMouseEvent &evt);

		// override these to provide the ontrol with data
		virtual void const *GetRootNode() = 0;
		virtual unsigned GetNumChildren(void const *handle) = 0;
		virtual void const *GetChild(void const *handle, int child) = 0;
		virtual void const *GetParent(void const *handle) = 0;
		virtual unsigned int GetValue(void const *handle) = 0;

		// the id can be different from the handle: the handle is probably unique for each tree you want to display
		// but the id could be the same for elements in different trees representing the same quantity.
		// maybe 'label' would be a better name. It sortof duplicates the GetName function, but for internal bookkeeping purposes
		virtual unsigned int GetId(void const *handle) = 0;
		virtual char const * GetName(void const *handle) = 0;
		unsigned int maxNumItems, numItems, selected;
		unsigned int maxLevel, maxValue;
		RadialPlotTreeItem *items;

		bool autoLevel;

		void ScanTree(void const *node, unsigned int level, double startAngle, double endAngle);

		void Grow()
		{
			maxNumItems *= 2;
			RadialPlotTreeItem *tmp = new RadialPlotTreeItem[maxNumItems];

			for (unsigned int i = 0; i < numItems; i++)
			{
				tmp[i] = items[i];
			}

			delete [] items;
			items = tmp;
		}

		void OnPaint(wxPaintEvent &evt);
		void OnSize(wxSizeEvent &evt);
		void OnErase(wxEraseEvent &evt);

		void Draw(wxDC *dc = NULL);


		unsigned int HitTest(int x, int y);
		unsigned int FindItem(void const *handle);
		unsigned int FindItem(unsigned int id);
		// aborts when given a handle not in the current tree
		unsigned int IdFromHandle(void const *handle);
		// returns NULL when the element with is id is not in the tree
		const void *HandleFromId(unsigned int id);

		wxPoint GetItemCentreWithIndex(unsigned int index);

		void DrawItems(wxDC *onto);


		void DrawItem(wxDC* onto, RadialPlotTreeItem const &item, wxPoint c, int diameter);

		#ifdef USE_CAIRO
		void  DrawItem(cairo_t* onto, RadialPlotTreeItem const &item, wxPoint c, int diameter);

		cairo_surface_t *surface;
		cairo_t *context;
		#endif

		wxBitmap *backBuffer;
	
};


#endif
