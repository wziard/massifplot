/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/
#include "radialplot.h"


BEGIN_EVENT_TABLE(RadialPlot, wxWindow)
    EVT_PAINT(RadialPlot::OnPaint)
    EVT_MOUSE_EVENTS(RadialPlot::OnMouse)
// 
//     EVT_MIDDLE_DOWN(ImageCanvas::MiddleDown)
//     EVT_RIGHT_DOWN(ImageCanvas::RightDown)
//     EVT_LEFT_UP(ImageCanvas::MouseUp)
//     EVT_MIDDLE_UP(ImageCanvas::MouseUp)
//     EVT_RIGHT_UP(ImageCanvas::MouseUp)
     EVT_SIZE(RadialPlot::OnSize)
// 	EVT_ENTER_WINDOW(ImageCanvas::MouseEnter)
// 	EVT_LEAVE_WINDOW(ImageCanvas::MouseLeave)
     EVT_ERASE_BACKGROUND(RadialPlot::OnErase)
// 	EVT_ACTIVATE(ImageCanvas::OnActivate)
END_EVENT_TABLE()


void RadialPlot::OnPaint(wxPaintEvent &WXUNUSED(evt) )
{
  wxPaintDC dc(this);
//! check dirtylist
  Draw(&dc);
}

void RadialPlot::OnErase(wxEraseEvent &WXUNUSED(event))
{
    // do nothing
}



void RadialPlot::OnSize(wxSizeEvent &WXUNUSED(evt))
{
	Refresh();
}

void RadialPlot::ScanTree(void const *node, unsigned int level, double startAngle, double endAngle)
{
	if (numItems >= maxNumItems)
		Grow();


	if (autoLevel && level > maxLevel)
		maxLevel = level;
		
	items[numItems].handle = node;
	items[numItems].level = level;
	items[numItems].startAngle = startAngle;
	items[numItems].endAngle = endAngle;
	items[numItems].id = GetId(node);
	items[numItems].color = GenerateElementColor(node);
	numItems++;

	int numChildren = GetNumChildren(node);
	unsigned int value = GetValue(node);
	double cAngle = startAngle;
	for (int i =0; i < numChildren ; i++)
	{
		void const *child = GetChild(node, i);
		unsigned int cValue = GetValue(child);
		double arc = (endAngle - startAngle) * cValue / value;
		ScanTree(child, level + 1, cAngle, cAngle + arc);
		cAngle += arc;
	}
}


void RadialPlot::DrawItem(wxDC * onto, RadialPlotTreeItem const &item, wxPoint c, int diameter)
{
	double r = ((double)diameter) * (item.level +1) / (2*(maxLevel + 1));

	double sa = item.startAngle;
	double ea = item.endAngle;

	double da = .01;

	sa += da;
	ea -= da;

	if (ea < sa)
		ea = sa = (sa+ea) / 2;


	int xstart = c.x  +  (int)(r * sin(sa));
	int ystart = c.y  -  (int)(r * cos(sa));
	int xend = c.x  +  (int)(r * sin(ea));
	int yend = c.y  -  (int)(r * cos(ea));

	wxBrush brush;
	brush.SetColour(item.color);
	onto->SetBrush(brush);

	if ((ea -sa) > .01 && !((xstart == xend) &&(ystart == yend)))
		onto->DrawArc(xend, yend, xstart, ystart, c.x, c.y);

	if ((sa < .1) && (ea > 2*M_PI - .1))
		onto->DrawCircle(c.x, c.y, (int) r);

	onto->DrawLine(c.x,c.y, xstart, ystart);
	onto->DrawLine(c.x, c.y, xend, yend);
}

#ifdef  USE_CAIRO
void RadialPlot::DrawItems(wxDC *bmp)
{
	int w = bmp->GetSize().x;
	int h = bmp->GetSize().y;
	wxPoint center(w/ 2, h / 2);

	int s = w > h ? h : w;


	if (surface)
	{
		int  cw = cairo_image_surface_get_width(surface);
		int  ch = cairo_image_surface_get_height(surface);
	 	if (cw != w || ch !=h )
		{
			cairo_destroy(context);
			cairo_surface_destroy(surface);
			surface = NULL;
		}
	}



	if (!surface)
	{
		surface = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w, h);
		context = cairo_create(surface);
	}
	else
	{
		cairo_set_operator(context, CAIRO_OPERATOR_SOURCE);
		cairo_set_source_rgba(context, 0,0,0,0);
		cairo_paint(context);
		cairo_set_operator(context, CAIRO_OPERATOR_OVER);
	}

	for (unsigned int i = maxLevel +1; i > 0 ; i--)
	{
		for (unsigned int j = 0; j < numItems; j++)
		{
			if (items[j].level == i - 1)
			{
				DrawItem(context, items[j], center, s);
			}
		}
	}

	cairo_surface_flush(surface);
//	cairo_surface_write_to_png(surface, "huk.png");

	wxBitmap temp(w,h);

	// due to a bug in wx rawbitmap access the OverlayImageSurface function only works if it is the last thing you draw on a bitmap
	// hence the copying around here
	// copy the current bitmap contents
	{
		wxMemoryDC tmpDc;
		tmpDc.SelectObject(temp);

		tmpDc.Blit(0,0, w, h, bmp, 0, 0);
	}

	OverlayImageSurface(surface, &temp);

	bmp->DrawBitmap(temp, 0, 0);

}


void  RadialPlot::DrawItem(cairo_t* cr, RadialPlotTreeItem const &item, wxPoint c, int diameter)
{
	double ro = ((double)diameter) * (item.level +1) / (2*(maxLevel + 1));
	double ri = ((double)diameter) * (item.level+.1) / (2*(maxLevel + 1));

	double sa = item.startAngle;
	double ea = item.endAngle;

	double da = .01;

	sa += da;
	ea -= da;

	if (ea < sa)
		ea = sa = (sa+ea) / 2;

	sa -= M_PI/2;
	ea -=M_PI/2;

	cairo_arc(cr, c.x, c.y, ro, sa, ea);
	cairo_arc_negative(cr, c.x, c.y, ri, ea, sa);

	cairo_close_path(cr);

	cairo_set_source_rgb(cr, item.color.Red()/255.0, item.color.Green()/255.0, item.color.Blue()/255.0);

	cairo_fill_preserve(cr);
	cairo_set_source_rgb(cr, 0, 0, 0);
	cairo_set_line_width(cr, 1);

	cairo_stroke(cr);

}


#else
void RadialPlot::DrawItems(wxBitmap *bmp)
{

	wxMemoryDC onto;
	onto.SelectObject(*bmp);

	wxSize s = onto.GetSize();
	wxPoint center(s.x/ 2, s.y / 2);

	if (s.x > s.y)
	{
		s.x = s.y;
	}
	else
	{
		s.y = s.x;
	}


	for (unsigned int i = maxLevel +1; i > 0 ; i--)
	{
		for (unsigned int j = 0; j < numItems; j++)
		{
			if (items[j].level == i - 1)
			{
				DrawItem(&onto, items[j], center, s.x);
			}
		}
	}
}
#endif

void RadialPlot::OnMouse(wxMouseEvent &evt)
{
	unsigned int hit = HitTest(evt.GetX(), evt.GetY());

	if (hit != selected)
	{
		selected = hit;
		Draw();
	}


	void const *handle = (hit != (unsigned int)(-1)) ? items[hit].handle : NULL;

	OnMouseEvt(evt, handle);
}



unsigned int RadialPlot::HitTest(int x, int y)
{
	// calc angle and radius
	wxSize c = GetClientSize();


	c.x /= 2;
	c.y /= 2;

	x -= c.x;
	y -= c.y;

	double angle = M_PI -atan2(x, y);
	double radius = hypot(x, y);

	double maxRadius =  c.x < c.y  ? c.x : c.y;

	unsigned int level = static_cast<int>(radius * (maxLevel+1) / maxRadius);


	for (unsigned int i = 0;i < numItems; i++)
	{
		if (items[i].level == level  && items[i].startAngle < angle && items[i].endAngle > angle)
		{
			return i;
		}
		
	}

	return (unsigned)(-1);
}


wxPoint RadialPlot::GetItemCentreWithIndex(unsigned int index)
{

	wxSize c = GetClientSize();


	c.x /= 2;
	c.y /= 2;

	if (index == (unsigned)(-1))
		return wxPoint(c.x, c.y);


	double maxRadius =  c.x < c.y  ? c.x : c.y;


	double radius = (items[index].level + .5) * maxRadius / (maxLevel + 1);
	double angle = (items[index].startAngle + items[index].endAngle) /2;



	return wxPoint((int)(c.x + radius * sin(angle)), (int)(c.y - radius * cos(angle)));

}

wxPoint RadialPlot::GetItemCentre(void const *handle)
{
	unsigned i = FindItem(handle);

	return GetItemCentreWithIndex(i);
}

wxPoint RadialPlot::GetItemCentre(unsigned int id)
{
	unsigned i = FindItem(id);

	return GetItemCentreWithIndex(i);
}



unsigned int RadialPlot::FindItem(void const *handle)
{
	for (unsigned int i = 0;i < numItems; i++)
	{
		if (items[i].handle == handle)
		{
			return i;
		}
		
	}

	return (unsigned)(-1);
}

unsigned int RadialPlot::FindItem(unsigned int id)
{
	for (unsigned int i = 0;i < numItems; i++)
	{
		if (items[i].id == id)
		{
			return i;
		}
		
	}

	return (unsigned)(-1);
}

unsigned int RadialPlot::IdFromHandle(void const *handle)
{
	unsigned int i = FindItem(handle);


	wxASSERT_MSG(i != (unsigned)(-1), "illegal id");

	return items[i].id;
}

const void *RadialPlot::HandleFromId(unsigned int id)
{
	unsigned i = FindItem(id);

	if (i == (unsigned)-1)
	{
		return NULL;
	}

	return items[i].handle;
}


void RadialPlot::Draw(wxDC *dc)
{
	bool mustDeleteDc = false;
	if (!dc)
	{
		dc = new wxClientDC(this);
		mustDeleteDc = true;
	}

	wxSize s = dc->GetSize();

	if (!backBuffer || s.x != backBuffer->GetWidth() || s.y != backBuffer->GetHeight())
	{
		if (backBuffer)
			delete backBuffer;
		backBuffer = new wxBitmap(s.x, s.y);
	}

	wxMemoryDC bdc;
	bdc.SelectObject(*backBuffer);



	bdc.SetFont(*wxNORMAL_FONT);
	bdc.SetPen(*wxBLACK_PEN);

	bdc.Clear();

	CustomDrawPre(&bdc);

	DrawItems(&bdc);


	int h = 0;
	bdc.GetTextExtent(wxT("A"), NULL, &h);

	wxString t;
	t.Printf(wxT("Full circle = %d"), maxValue );

	bdc.DrawText(t, 1, bdc.GetSize().y - h - 1);

	if (selected != (unsigned int)(-1))
	{
		bdc.DrawText(wxString::FromAscii(GetName(items[selected].handle)), 1,1);

		wxString t;
		t.Printf(wxT("%d\n"), GetValue(items[selected].handle));

		bdc.DrawText(t, 1,20);

	}

	CustomDrawPost(&bdc);

	bdc.SelectObject(wxNullBitmap);

	dc->DrawBitmap(*backBuffer,0,0);

	if (mustDeleteDc)
	{
		delete dc;
	}
}


void RadialPlot::RefreshTree(void const *startNode, int _maxLevel, int _maxValue)
{
	if (!startNode)
		startNode = GetRootNode();

	if (!startNode)
		return;

	numItems = 0;

	maxLevel = _maxLevel;

	autoLevel = !maxLevel;

	maxValue  = _maxValue;

	unsigned int val = GetValue(startNode);


	if (!val)
		val = 1;

	if (val > maxValue)
	{
		maxValue = val;
	}

	ScanTree(startNode, 0, 0, M_PI * 2 * val / maxValue);
}


unsigned int RadialPlot::GenerateColor(unsigned int c)
{
	unsigned int r =200 * (int)(50 * sin(c * .01));
	unsigned int g =200 * (int)(50 * sin(c * .012));
	unsigned int b =200 * (int)(50 * sin(c * .011));
	

	return (r << 16) + (g << 8) + b;
}

