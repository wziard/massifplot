#include "expatimpl.h"
#include <stdio.h>

class XMLTree
{
    public:
      XMLTree();
      ~XMLTree();
      void SetName(char const *name, int nameSize = 0);
      char const *GetName() const;
      void AppendValue(char const *value, int size = 0);
      void ClearValue();

      void SetValue(char const *value) { ClearValue(); AppendValue(value); }
      char const *GetValue()  const;
      
      char const *GetAttribute(char const *attribute) const;
      void SetAttribute(char const *attribute, char const *value);
      
      void AppendChild(XMLTree *child);
      int GetNumChildren()  const;
      XMLTree *GetChild(int i);
      void RemoveChild(int i);
      XMLTree *GetParent()  const;
      bool HasValue()  const { return value != NULL; }

      void Print(FILE *to, bool onlyChildren = true, int indent = 0, bool autoIndent = true)  const;

      XMLTree *FindOrCreateChild(char const *name, int nameSize = 0);
      XMLTree *FindChild(char const *name , int nameSize = 0)  const;
      
      void Trim(); // trims all whites[ace at the start & end of value
    private:
      void AddAttribute(char const *a, char const *v);
      XMLTree *FindAttribute(char const *a) const;
      void PrintEscaped(FILE *to, char const *string) const;
      void PrintAttributes(FILE *to) const;

      void GrowChildren();
      int maxNumChildren;
      int numChildren;
      void GrowAttributes();
      int maxNumAttributes;
      int numAttributes;

      
      char *name;
      char *value;
      XMLTree **children;
      XMLTree *parent;

      XMLTree **attributes;

      
};


class CMyXML : public CExpatImpl <CMyXML> 
{
public:

	// Constructor 
	
	CMyXML () 
	{
        tree = NULL;
        curLocation = NULL;
	}
    
	bool ParseFile(FILE *out, bool ignoreWhiteSpace = true);
	bool ParseCString(char const * buffer, bool ignoreWhiteSpace = true, bool isLast = true);
    
	// Invoked by CExpatImpl after the parser is created
	
	void OnPostCreate ()
	{
		// Enable all the event routines we want
		EnableStartElementHandler ();
		EnableEndElementHandler ();
		// Note: EnableElementHandler will do both start and end
		EnableCharacterDataHandler ();

        tree = new XMLTree;
        curLocation = tree;
        
	}
	
	// Start element handler

	void OnStartElement (const XML_Char *pszName, const XML_Char **papszAttrs)
	{
        XMLTree *child = new XMLTree;
        child->SetName(pszName);
        curLocation->AppendChild(child);
        AddAttributesToChild(child, papszAttrs);
        curLocation = child;
		return;
	}

	// End element handler

	void OnEndElement (const XML_Char *PARAM_UNUSED(pszName))
	{
        if (ignoreWhiteSpace)
            curLocation->Trim();
            
        curLocation = curLocation->GetParent();

		return;
	}

	// Character data handler

	void OnCharacterData (const XML_Char *pszData, int nLength)
	{
		// note, pszData is NOT null terminated
        //! if whitespace
        
        curLocation->AppendValue(pszData, nLength);
        
		return;
	}

    XMLTree *GetCurTree()
    {
        return tree;
    }

    private:
      void AddAttributesToChild(XMLTree *child, XML_Char const **array)
      {
        if (!array)
            return;
            
        int count = 0;

        while(array[2*count])
        {
//            printf(">%s<   =   >%s<\n",array[2*count], array[2*count+1]);
            child->SetAttribute(array[2*count], array[2*count+1]);
            count++;
        }
      }
    
      XMLTree *tree;
      XMLTree *curLocation;
      bool ignoreWhiteSpace;
};
