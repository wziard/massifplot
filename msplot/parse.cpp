/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/
#include "parse.h"
#include <stdio.h>
#include <ctype.h>
#define BUFSIZE 10000

static char buf[BUFSIZE];
static FILE *theFile = NULL;
static bool endOfFile = false;
static bool dontRead = false;
static unsigned int lineNo = 0;

static void UnReadLine()
{
	dontRead = true;
}

static void ReadLine()
{

	if (!dontRead)
	{
		char *r = fgets(buf, BUFSIZE, theFile);

		if (!r)
		{
			buf[0] = 0;
			return;
		}

		int len = strlen(buf);
		len--;

		while(len >= 0 && isspace(buf[len]))
			len--;

		len++;
		buf[len] = 0;
		
		lineNo++;
	}
	dontRead = false;
	endOfFile = feof(theFile);
}



static void Perror(char const *msg, char const *error)
{
	printf("line %d: %s\n%s\n", lineNo, msg, error);
	exit(1);
}


static bool MatchInt(char const *key,unsigned int *store, char const *src)
{
	int len = strlen(key);
	if (strncmp(key, src, len))
		return false;

	if (store)
	{
		*store = strtoul(src+len, NULL, 0);
	}

	return true;
}

static bool MatchString(char const *key, char const **store, char const *src)
{
	int len = strlen(key);
	if (strncmp(key, src, len))
		return false;

	if (store)
	{
		*store = src+len;
	}

	return true;
}


static bool MatchTreeNode(unsigned int *numChildren, unsigned int *memAmount, char const **name, char const *buf)
{
	char convertBuf[BUFSIZE];
	int start =0;

	while(buf[start] == ' ')
		start++;

	if (buf[start] != 'n')
		return false;

	start++;
	int end = start;

	while (buf[end] >= '0' && buf[end] <= '9')
		end++;

	if (start == end)
		return false;

	for (int i = 0; i < end - start; i++)
		convertBuf[i] = buf[start+i];

	convertBuf[end - start] = 0;

	if (numChildren)
	{
		*numChildren = strtoul(convertBuf,NULL,0);
	}

	start = end;


	if (buf[start] != ':')
		return false;

	start++;


	if (buf[start] != ' ')
		return false;

	start++;

	end = start;

	while (buf[end] >= '0' && buf[end] <= '9')
		end++;

	if (start == end)
		return false;
		


	for (int i = 0; i < end - start; i++)
		convertBuf[i] = buf[start+i];

	convertBuf[end - start] = 0;

	if (memAmount)
	{
		*memAmount = strtoul(convertBuf,NULL,0);
	}

	start = end;

	if (buf[start] != ' ' )
		return false;

	if (name)
	{
		*name = buf +start + 1;
	}

	return true;
}

static char const *PrettifyName(char const *name)
{
	if (name[0] != '0')
	{
		return name;
	}

	while(*name && !isspace(*name))
	{
		name++;
	}

	if (*name)
		name++;

	return name;

}

// reads all children (and *their* children) frrom a tree node
static void ReadChildren(TreeNode *parent, int num)
{
	unsigned int numChildren, memAmount;
	char const *name;
	
	for (int i = 0; i < num ; i++)
	{
		ReadLine();

		if (endOfFile)
			Perror("Premature", "end of file");

		if (MatchTreeNode(&numChildren, &memAmount, &name, buf))
		{
			bool isInSignificant = strstr(name, "all below massif's threshold");

			TreeNode *t = new TreeNode(memAmount, PrettifyName(name), isInSignificant);

			parent->AddChild(t);

			if (numChildren)
				ReadChildren(t, numChildren);
		}
		else
		{
			Perror("expected tree node instead of", buf);
		}
	}
}


static SnapShot *ReadSnapShot(int snr)
{
	unsigned int nr, memAmount,numChildren;
	char const *name;


	SnapShot *s = new SnapShot(snr);
	
	while(1)
	{
		ReadLine();

		if (endOfFile)
			break;
			
		if (buf[0] == '#')  // skip comments
			continue;

		if (MatchInt("time=", &nr, buf))
		{
			s->SetTime(nr);
		}
		else if (MatchInt("mem_heap_B=", &nr, buf))
		{
			s->SetHeap(nr);
		}
		else if (MatchInt("mem_heap_extra_B=", &nr, buf))
		{
			s->SetHeapExtra(nr);
		}
		else if (MatchInt("mem_stacks_B=", &nr, buf))
		{
			s->SetStack(nr);
		}
		else if (MatchString("heap_tree=", NULL, buf))
		{
			// ignore
		}
		else if (MatchTreeNode(&numChildren, &memAmount, &name, buf))
		{
			TreeNode *t = new TreeNode(memAmount, name, false);
			ReadChildren(t, numChildren);

			s->AddAllocTree(t);
		}
		else
		{
			UnReadLine();
			break;
		}
		

	}
	return s;
}


Session *ReadMassifFile(FILE *in)
{
	theFile = in;
	char const *match = NULL;
	unsigned int nr;

	Session *session = new Session;

	while(1)
	{
		ReadLine();
		if (endOfFile)
			break;

		if (buf[0] == '#')  // skip comments
			continue;

		if (MatchString("desc:", &match,buf))
		{
			session->SetDesc(match);
		}
		else if (MatchString("cmd:", &match, buf))
		{
			session->SetCmd(match);
		}
		else if (MatchString("time_unit:", &match, buf))
		{
			session->SetTimeUnit(match);
		}
		else if (MatchInt("snapshot=", &nr, buf))
		{
			session->AddSnapShot(ReadSnapShot(nr));
		}
		else
		{
			Perror("Unknown line:",buf);
		}





	}


	session->DoFinalBookKeeping();


	return session;
}


// this is used to build up the global tree
//  it checks if this node already contains a child of the same name, and if so recursively merges the children
// or if it doen's yet exist it clones them
void TreeNode::MergeChild(TreeNode const *t)
{
	for (int i = 0; i < numChildren ;i++)
	{
		if ((children[i]->isInsignificant && t->isInsignificant) || !strcmp(children[i]->name, t->name))
		{
			// since we only look at the name, the size info of these nodes will be meaningless
			for (int j =0; j < t->numChildren; j++)
			{
				children[i]->MergeChild(t->children[j]);
			}
			return;
		}
	}

	AddChild(t->Clone());
	
}

