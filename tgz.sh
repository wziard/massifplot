#!/bin/sh
echo  `date +"%d-%b-%Y"` > msplot/versiondate.txt
name=msplot-`date +"%d-%b-%Y"`.tgz
make -C msplot veryclean
cp msplot/usage.txt website
tar -zcvf $name msplot
mv $name website/downloads
cd website/downloads
./generatehtmlindex.sh

