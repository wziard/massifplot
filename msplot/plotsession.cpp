/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/
#include "plotsession.h"


/* hsv_to_rgb:
 *  Converts from HSV colorspace to RGB values.
 *      nicked this function from the (great) Allegro library
 * where it has this comment:
 *      Dave Thomson contributed the RGB <-> HSV conversion routines.
 */
void hsv_to_rgb(float h, float s, float v, int *r, int *g, int *b)
{
   float f, x, y, z;
   int i;

//   ASSERT(s >= 0 && s <= 1);
//   ASSERT(v >= 0 && v <= 1);

   v *= 255.0f;

   if (s == 0.0f) { /* ok since we don't divide by s, and faster */
      *r = *g = *b = v + 0.5f;
   }
   else {
      h = fmod(h, 360.0f) / 60.0f;
      if (h < 0.0f)
	 h += 6.0f;

      i = (int)h;
      f = h - i;
      x = v * s;
      y = x * f;
      v += 0.5f; /* round to the nearest integer below */
      z = v - x;

      switch (i) {

	 case 6:
	 case 0:
	    *r = v;
	    *g = z + y;
	    *b = z;
	    break;

	 case 1:
	    *r = v - y;
	    *g = v;
	    *b = z;
	    break;

	 case 2:
	    *r = z;
	    *g = v;
	    *b = z + y;
	    break;

	 case 3:
	    *r = z;
	    *g = v - y;
	    *b = v;
	    break;

	 case 4:
	    *r = z + y;
	    *g = z;
	    *b = v;
	    break;

	 case 5:
	    *r = v;
	    *g = z;
	    *b = v - y;
	    break;
      }
   }
}


void const *SessionPlot::GetRootNode()
{
	if (!session)
		return NULL;

	return session->GetSnapShot(snapShot)->GetAllocTree();
}


unsigned int SessionPlot::GetNumChildren(void const *handle)
{
	TreeNode const *t = (TreeNode const *)handle;

	if (t->GetNumLeaves() == 1 && t->GetBranchDepth() >= singleChildDepth)
	{
		return 0;
	}

	return t->GetNumChildren();
}


void const *SessionPlot::GetChild(void const *handle, int child)
{
	TreeNode const *t = (TreeNode const *)handle;
	return t->GetChild(child);
}

void const *SessionPlot::GetParent(void const *handle)
{
	TreeNode const *t = (TreeNode const *)handle;
	return t->GetParent();
}

unsigned int SessionPlot::GetValue(void const *handle)
{
	TreeNode const *t = (TreeNode const *)handle;
	return t->GetSize();
}


unsigned int SessionPlot::GetId(void const *handle)
{
	TreeNode const *t = (TreeNode const *)handle;
	return t->GetId();
}

char const *SessionPlot::GetName(void const *handle)
{
	TreeNode const *t = (TreeNode const *)handle;
	return t->GetName();

}

void SessionPlot::OnMouseEvt(wxMouseEvent &evt, void const *handle)
{
	if (!handle)
		return;

	TreeNode const *t = (TreeNode const *)handle;

	if (evt.LeftDown())
	{
		numDisplayedIds++;
		if (numDisplayedIds > MAXNUMDISPLAYEDIDS)
			numDisplayedIds = MAXNUMDISPLAYEDIDS;

		for (int i = numDisplayedIds; i > 0; i--)
		{
			displayedIds[i] = displayedIds[i - 1];
		}

		displayedIds[0] = IdFromHandle(handle);
		DoRefresh();
	}
	else if (evt.LeftDClick())
	{
		unsigned int old = rootNodeId;
		rootNodeId = t->GetId();

		if (old == rootNodeId)
		{
			TreeNode const *p = t->GetParent();

			rootNodeId = p ? p->GetId() : 0;
		}


		DoRefresh();
	}

}

void  SessionPlot::CustomDrawPost(wxDC *dc)
{
	int h = 0;
	dc->GetTextExtent(wxT("A"), NULL, &h);
	wxString txt;

	h+=2;

	for (int i = 0; i < numDisplayedIds; i++)
	{
		TreeNode const *t = (TreeNode const *)HandleFromId(displayedIds[i]);

		if (!t)
		{
			continue;
		}
		
		txt = wxString::FromAscii( t->GetName());

		dc->DrawText(txt,1, 60 + h * i);

		wxPoint s;

		dc->GetTextExtent(txt, &(s.x), &(s.y));

		s.x += 2;
		s.y /=2 ;
		s.y += (60 + h * i);

		wxPoint e = GetItemCentre(t->GetId());

		dc->DrawLine(s, e);

	}
}

wxColor SessionPlot::GenerateElementColor(void const *handle)
{
	char const *name = ((TreeNode const *)handle)->GetName();

	int len = strlen(name);

	int huehash = 0;

	for (int  i = 0; i < len ; i++)
	{
		for (int bit = 0; bit < 8; bit += 2)
		{
			// use the nr of bits in the name to get a hue
			huehash += (name[i] & (1 << bit)) >> bit;
		}
	}
	float v = .6 + .4 * cos(len * .2);	// v is
	float h = 3.0 * huehash;
	float s = .4 + .6 * exp(-len/200.0);


	int r,g,b;

	hsv_to_rgb(h,s,v, &r, &g, &b);

	return wxColor(r,g,b);
}

