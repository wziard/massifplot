/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009,2010 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/
#ifndef __STACKED_GRAPH_H__
#define __STACKED_GRAPH_H__

#include <wx/window.h>
#include <wx/bitmap.h>

class StackedGraph
	: public wxWindow
{
	public:
		StackedGraph(wxWindow *parent);

		virtual ~StackedGraph();
		// must provide the number of points on the X axis
		virtual unsigned GetNumSamples()
		{
			return 0;
		}

		// provides the number of 'stackables'  at this sample
		virtual unsigned GetNumData(unsigned sample)
		{
			return 0;
		}

		// provides the value of this stackable, these will be drawn with 0 at the top
		virtual unsigned GetValue(unsigned sample, unsigned datum)
		{
			return 0;
		}

		// datums with the same id will be connected horizontally
		virtual unsigned GetId(unsigned sample, unsigned datum)
		{
			return 0;
		}

		virtual wxColor GetFillColor(unsigned sample, unsigned datum)
		{
			return *wxWHITE;
		}
		
		virtual wxColor GetLineColor(unsigned sample, unsigned datum)
		{
			return *wxBLACK;
		}
		
		virtual void  CustomDrawPre(wxDC *bitmap)
		{
		}
		
		virtual void  CustomDrawPost(wxDC *bitmap)
		{

		}

	protected:
		// you can provide a faster version if you have the data readily available
		virtual unsigned GetMaxTotalValue()
		{
			unsigned totalValue = 0;
			for (unsigned i = 0; i < GetNumSamples(); i++)
			{
				unsigned localValue = 0;
				for (unsigned j = 0; j < GetNumData(i); j++)
				{
					localValue += GetValue(i,j);
				}

				if (localValue > totalValue)
				{
					totalValue = localValue;
				}
			}

			return totalValue;
		}

		void OnPaint(wxPaintEvent &evt);
		void OnSize(wxSizeEvent &evt);
		void OnErase(wxEraseEvent &evt);

		void Draw(wxDC *dc = NULL);
		void OnMouse(wxMouseEvent &evt);
		DECLARE_EVENT_TABLE();

		wxBitmap m_backBuffer;

};


#endif
