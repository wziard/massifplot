/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/
#ifndef __PLOTSESSION_H__
#define __PLOTSESSION_H__

#include "radialplot.h"
#include "parse.h"

// this file implements a version of the radialPlotwidget which gets it's data from a TreeNode tree.

#define MAXNUMDISPLAYEDIDS 10


class SessionPlot
	: public  RadialPlot
{
	public:
		SessionPlot(wxWindow *parent)
			: RadialPlot(parent)
		{
			session = NULL;
			snapShot = 0;
			masterMaxLevel = masterMaxValue = 0;
			rootNodeId = 0;
			numDisplayedIds = 0;
			singleChildDepth = 1;
		}

		void SetSession(Session *_session)
		{
			session = _session;
			DoRefresh();
		}

		void SetSnapShot(unsigned int s)
		{
			snapShot = s;
			DoRefresh();
		}

		void SetMaxLevel(unsigned int max)
		{
			masterMaxLevel = max;
			DoRefresh();
		}

		void SetMaxValue(unsigned int max)
		{
			masterMaxValue = max;
			DoRefresh();
		}

		void DoRefresh(bool redraw = true)
		{
			TreeNode const *rootNode = rootNodeId? session->FindNode(snapShot, rootNodeId) : NULL;
			RefreshTree(rootNode, masterMaxLevel, masterMaxValue);
			Refresh();
		}

		void SetSingleChildDepth(unsigned int maxlevel)
		{
			singleChildDepth = maxlevel;
			DoRefresh();
		}

		void OnMouseEvt(wxMouseEvent &evt, void const *handle);
		virtual void  CustomDrawPost(wxDC *bitmap);

		virtual wxColor GenerateElementColor(void const *handle);
	private:
		void const *GetRootNode();
		unsigned int GetNumChildren(void const *handle);
		void const *GetChild(void const *handle, int child);
		void const *GetParent(void const *handle);
		unsigned int GetValue(void const *handle);
		unsigned int GetId(void const *handle);
		char const *GetName(void const *handle);

		unsigned int masterMaxValue, masterMaxLevel;

		unsigned int rootNodeId;


		unsigned int displayedIds[MAXNUMDISPLAYEDIDS];
		int numDisplayedIds;

		unsigned int singleChildDepth;
		Session *session;
		unsigned int snapShot;
};



#endif
