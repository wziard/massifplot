/*
   This file is part of msplot, a visualisation tool for Massif output

   Copyright (C) 2007,2008,2009,2010 Martijn Versteegh


   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   This program is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA
   02111-1307, USA.

   The GNU General Public License is contained in the file COPYING.
*/

#include "stackedgraph.h"
#include <wx/dcclient.h>

BEGIN_EVENT_TABLE(StackedGraph, wxWindow)
    EVT_PAINT(StackedGraph::OnPaint)
    EVT_MOUSE_EVENTS(StackedGraph::OnMouse)
    EVT_SIZE(StackedGraph::OnSize)
    EVT_ERASE_BACKGROUND(StackedGraph::OnErase)
END_EVENT_TABLE()


StackedGraph::StackedGraph(wxWindow *parent)
	: wxWindow(parent, -1)
{

}


StackedGraph::~StackedGraph()
{
}

void StackedGraph::OnPaint(wxPaintEvent &WXUNUSED(evt) )
{
  wxPaintDC dc(this);
//! check dirtylist
  Draw(&dc);
}

void StackedGraph::OnErase(wxEraseEvent &WXUNUSED(event))
{
    // do nothing
}



void StackedGraph::OnSize(wxSizeEvent &WXUNUSED(evt))
{
	wxSize s = GetClientSize();
	m_backBuffer.Create(s.x, s.y);
	Refresh();
}

void StackedGraph::OnMouse(wxMouseEvent &evt)
{
	//clicking on the graph should be like clicking on a slider

}

void StackedGraph::Draw(wxDC *dc)
{
}
